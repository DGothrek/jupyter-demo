# Introduction

Cette présentation a deux buts principaux :

+ installer Jupyter sur votre ordinateur correctement
+ donner quelques idées des possibilités que cela implique

## Installation

L'installation comprend deux parties principales:

+ installation de Python
+ installation des packages nécessaires pour Jupyter

Voir la page [Installation](local-install.html) pour plus de détails.

:::tip NB
Il existe des manières d'utiliser Jupyter à distance via un [hub](https://jupyterhub.readthedocs.io/en/stable/)
ou [myBinder.org](https://myBinder.org), mais pour une utilisation avec sauvegarde
et sans calculs lourds, il est préférable d'opter pour une installation locale.
:::
