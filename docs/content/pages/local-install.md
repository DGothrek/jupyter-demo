# Installation

## Installation de Miniconda

[Miniconda](https://conda.io/miniconda.html) est une version minimale d'[Anaconda](https://www.anaconda.com/what-is-anaconda/),
la plus répandue des distributions [Python](https://www.python.org/).

Nous présenterons ici une utilisation avec Python3, et nous recommandons fortement
d'y passer car [Python2 ne sera bientôt plus maintenu](https://pythonclock.org/)
(dans le doute, réinstaller miniconda).

Un installeur est trouvable sur la page de téléchargements de [Miniconda](https://conda.io/miniconda.html).

![miniconda download](../img/miniconda-download.png)

:::tip NB
Miniconda est recommandé car plus léger qu'Anaconda, ainsi la gestion correcte des
environnements y est plus simple, mais en pratique les différences sont faibles.
:::

Lancez ensuite l'installeur en double-cliquant ou via la console pour Mac/Linux.

```bash
# macOS
$ cd ~/Downloads
$ bash Miniconda3-latest-MacOSX-x86_64.sh
````

```bash
# Linux
$ cd ~/Downloads
$ bash Miniconda3-latest-Linux-x86_64.sh
```

:::warning
Pour avoir accès à Python dans le terminal, il est **important de sélectionner l'option
"add to PATH"** (non recommandée normalement mais essentielle ici).
:::

## Vérification de l'installation

Pour vérifier si l'installation est correcte, ouvrez le terminal:

Pour les utilisateurs de Windows, cliquez sur le menu démarrer et cherchez "cmd".
Lancez ensuite l'invite de commandes Windows.

+ Windows:

```bash
# vérification de l'installation de conda
> conda --version
conda 4.5.4

# vérification de Python
> where python
...\Miniconda3\python.exe

```

+ macOS ou Linux:

```bash
# vérification de conda
$ conda --version
conda 4.5.4

# Vérification de Python
$ which python
/Users/Louis/miniconda3/bin/python
```

## Téléchargement des fichiers de démo

2 options :

1. Clonez le [repo](https://github.com/PierreMarion23/prez-Jupyter-X)si vous avez [git](https://git-scm.com/).

```bash
$ git clone https://github.com/PierreMarion23/prez-Jupyter-X
```

2. Téléchargez le .zip du [repo git](https://github.com/PierreMarion23/prez-Jupyter-X) comme montré si dessous.

![zip](../img/download-zip.png)

Puis décompressez-le.

## Créer un environnement Python

Pour gérer correctement les packages Python utilisés en fonction des projets, et
ne pas surcharger l'environnement, il est nécessaire d'utiliser des environnements
python spécifiques pour chaque type de projet.

On utilise ici conda comme gestionnaire de packages python, que l'on installe
depuis un fichier environment.yml, mais on peut aussi utiliser pip pour gérer ces
packages.

Pout Windows, macOS, Linux:

```bash
# move to the notebooks folder
$ cd prez-Jupyter-X

# create env
$ conda env create -f environment.yml

# enter env - if Windows
$ activate example-environment
# enter env - if macOS or Linux
$ source activate example-environment

# check which python is used in env - if Windows
(example-environment) $ where python
...\Miniconda3\envs\jupyter-demo\bin\python.exe
# check which python is used in env - if macOS or Linux
(example-environment) $ which python
/Users/Louis/miniconda3/envs/jupyter-demo/bin/python
# you should see a path in the jupyter-demo env

# check env
(example-environment) $ conda list
# you should see many packages including those above
(example-environment) $ conda list jupyter

# leave env
# (jupyter-demo) $ deactivate
```
