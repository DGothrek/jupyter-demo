
# Notebooks Jupyter

## Bases

### Lancer un serveur notebook

Sur le disque, un notebook [Jupyter](http://jupyter.org/) est un fichier `.ipynb`.

Pour utiliser les notebooks:

```bash
# select example-environment env
$ activate example-environment # if Windows
$ source activate example-environment # if macOS or Linux

# move to folder containing notebooks
$ cd your/path/to/notebooks

# launch notebook server
(example-environment) $ jupyter notebook
```

On doit pouvoir avoir accès à l'ensemble des fichiers présents:

<img src="../img/jupyter-first-nb.png" alt="" width="100%" style="display: block; margin: auto;"> 

Cliquer sur un notebook pour l'ouvrir.  
Sélectionnez le fichier `prezX-0.ipynb` to start.  
Une nouvelle tabulation est alors disponible dans votre navigateur:

<img src="../img/jupyter-file-explorer.png" alt="" width="100%" style="display: block; margin: auto;"> 

### Cells

Un notebook peut contenir des cells de deux types différents

+ markdown
+ code

### Navigation

Pout exécuter une cellule, cliquer dessus et utiliser une des méthodes suivantes:

+ `Shift+Enter` : exécute la cellule et sélectionne la suivante
+ `Ctl+Enter`: exécute la cellule et y reste
+ `Alt+Enter`: exécute la cellule et crée une cellule vide en dessous
+ cliquer sur le bouton "Run" dans le menu

Il y a deux modes de navigation:

+ **édition**: Cliquer ou double-cliquer sur une cellule pour la modifier. a cellule est alors entourée en vert.
+ **commandes**: Exécuter une cellule ou appuyer sur "Esc" pour y accéder. La cellule est entourée en bleu.

![command-mode](../img/jupyter-command-mode.png)

Pour naviguer entre les cellules, on peut utiliser :

+ la souris das tous les cas
+ les flèches haut/bas en command mode

Pour ajouter une cellule au notebook, en command mode:

+ appuyer sur `a` pour une cellule au dessus (above)
+ appuyer sur `b` pour une cellule en dessous (below)

Pour supprimer une cellule:

+ appuyer deux fois sur `d`

### Kernel

L'exécution des cellules est faite dans un kernel Python, qui est unique pour
chaque notebook.  

Pour recharger le kernel et lancer l'exécution de toutes les cellules,
appuyer sur :fast_forward: dans le menu.

:::tip
Tous les raccourcis sont disponible en appuyant sur `h` en command mode.
:::

### Callules Markdown

Markdown est un outil puissant et **simple** (cc LaTeX) de mise en forme de texte.
Il est assez pratique et en même temps permet des résultats puissants.

Example: A markdown cell in

+ edit mode: click on cell

<img src="../img/jupyter-md-edit.png" alt="" width="75%" style="display: block; margin: auto;"> 

+ command mode: run cell

<img src="../img/jupyter-md-show.png" alt="" width="75%" style="display: block; margin: auto;"> 

Markdown permet d'afficher des images, des liens, des tableaux, des listes, des
formules LaTeX, du code...

Pour plus d'exemples et de détails, voir cette [markdown cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet).

### Cellules de code

Les cellules de code se divisent en deux parties:

+ Une partie input, où l'utilisateur écrit le code
+ Une partie output où le résultat de l'input est affiché

L'output peut afficher tout ce qu'une page web peut afficher: texte, images,
animations JavaScript...

## Packages utiles et puissance du notebook

Ci dessous sont quelques packages utiles et spécifiques au notebook, mais on peut
en imaginer beaucoup d'autres.

### Packages graphiques

Voici plusieurs packages parmi les plus utilisés, avec un potentiel important
dans le notebook:
+ [matplotlib](https://matplotlib.org/): une des plus vieilles librairies, mais toujours efficace
+ [seaborn](https://seaborn.pydata.org/): Pour de la visualisation statistique
+ [ezhc](https://github.com/oscar6echo/ezhc): encapsule la librairie [highcharts](https://www.highcharts.com/), une librairie JavaScript très populaire
+ [bqplot](https://github.com/bloomberg/bqplot): une librairie de **graphes interragissant avec Python** en utilisant ipywidgets
+ [bokeh](https://bokeh.pydata.org/en/latest/): une librairie très puissante, dans le même genre que seaborn
+ [plotly](https://plot.ly/python/): idem seaborn et bokeh

En général, ces librairies sont **très bien documentées** avec de nombreux exemples,
et il est très facile d'obtenir un résultat spectaculaire avec un minimum de
lecture de documentation.

### IPywidgets

[ipywidgets](https://ipywidgets.readthedocs.io/en/stable/) Va encore plus loin
en faisant le lien entre le JavaScript et les objets Python, permettant une
utilisation interactive d'objets Python.
Ils permettent de construire un GUI dans un notebook. Pout plus de détails, voir
la [liste des widgets core](https://ipywidgets.readthedocs.io/en/stable/examples/Widget%20List.html).

D'autres widgets custom permettent d'encapsuler complètement des librairies JS
élaborées, comme bqplot que nous avons cité plus haut, ou
[ipyaggrid](https://dgothrek.gitlab.io/ipyaggrid/) pour la
librairie [ag-Grid](https://www.ag-grid.com/).
