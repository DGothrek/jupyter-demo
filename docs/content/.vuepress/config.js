module.exports = {
    title: 'Introduction à Jupyter',
    description: '',
    base: '/jupyter-demo/', // Comment if in dev mode
    dest: '../public',
    head: [['link', { rel: 'icon', href: '/favicon.ico' }]],
    // serviceWorker: true,
    themeConfig: {        
        repo: 'https://gitlab.com/dgothrek/jupyter-demo',
        editLinks: true,
        editLinkText: 'Edit this page on GitLab',
        sidebarDepth: 5,
        sidebar: [
            '/pages/introduction',
            '/pages/local-install',
            '/pages/jupyter',
        ],
    },
    markdown: {
        lineNumbers: false,
    },
};
