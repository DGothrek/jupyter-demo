---
home: true
heroText: 'Introduction à Jupyter'
description: '2 octobre 2018, École polytechnique'
heroImage: '/jupyter.png'
actionText: Utiliser Jupyter →
actionLink: /pages/introduction
features:
- title: Installation de Python
  details: Installation de Python depuis le début, gestion d'environnements.
- title: Jupyter
  details: Installation et bases des notebooks Jupyter.
- title: Utilisations avancées
  details: Démos avancées sur les notebooks, les labs...
footer: Présentation - Pierre Marion, Olivier Coudray (X15) Flavien Solt et Louis Raison (X16)
pageClass: custom-home-page
---
